import Rx from 'rxjs';

export default function watchPosition(options) {
    return Rx.Observable.create(observer => {
        let watchId = window.navigator.geolocation.watchPosition(
            loc => observer.next(loc),
            err => observer.error(err),
            options);

        return () => {
            window.navigator.geolocation.clearWatch(watchId);
            console.log('Cancel');
        };
    }).share();
}
