import '../css/style.css';
import GoogleMapsLoader from 'google-maps';
import watchPosition from './watchPosition';
import {darkStyle} from './mapStyles';
import io from 'socket.io-client';

var socket = io('ws://' + location.host + ':8080');

console.log(socket.id); // Undefined
socket.on('connect', function () {
    console.error(socket.id); // 'G5p5...'
});

const coords = [
    {
        latitude: '56.8354929',
        longitude: '60.590235199999995'
    },
    {
        latitude: '56.83548298501805',
        longitude: '60.59007232057761'
    },
    {
        latitude: '56.8354730700361',
        longitude: '60.58990944115523'
    }
];

GoogleMapsLoader.KEY = 'AIzaSyA1e0_eBW7zZghkvUNjHZgAOZ_Zmm4fLjg';
GoogleMapsLoader.load(google => {
    if (!navigator.geolocation) {
        return;
    }

    const output = document.getElementById('output');
    const map = new google.maps.Map(window.document.getElementById('map'), {
        center: new google.maps.LatLng(coords[0].latitude, coords[0].longitude),
        zoom: 17,
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: darkStyle
    });
    coords.forEach((coord, index) => {
        new google.maps.Circle(
            {
                center: new google.maps.LatLng(coord.latitude, coord.longitude),
                strokeColor: index === 0 ? '#FF00FF' : '#0000FF',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                radius: 5,
                fillColor: index === 0 ? '#FF00FF' : '#0000FF',
                fillOpacity: 0.86
            }).setMap(map);
    });

    const source = watchPosition({
        enableHighAccuracy: true,
        maximumAge: 30000
    });

    source.subscribe(
        position => {
            output.innerText = `latitude:${position.coords.latitude} longitude: ${position.coords.longitude} accuracy: ${position.coords.accuracy} altitude: ${position.coords.altitude} speed: ${position.coords.speed}`;
        },
        err => {
            output.innerText = `ERROR: ${err.code} ${err.message}`;
        }
    );

    return map;
});
