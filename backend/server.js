var uuid = require('uuid-js');
var express = require('express');
var app = require('express')();
var path = require('path');

var server = require('http').Server(app);
var io = require('socket.io')(server);

var port = process.env.PORT || 8080;


app.use(express.static(__dirname + '/../dist'));
app.listen(port);

app.get('/', function (req, res) {
    res.cookie('client_id',  uuid.create().toString());
    res.sendfile(path.resolve(__dirname + '/../dist/index.html'));
});

io.on('connection', function (socket) {
    socket.on('getData', function (uuid, location) {
        sendResponse(socket, 'fullState', fakeFullData);
        sendBroadcast(socket, 'userState', fakePartData);
    });

    socket.on('move', function (point) {
        sendBroadcast(socket, 'setData', fakePartData);
    });
});

// apples in setInterval

function sendResponse (socket, event, data) {
    socket.send(event, {
        data: data
    });
}

function sendBroadcast (socket, event, data) {
    socket.emit(event, {
        data: data
    });
}

var fakePartData = {
    snakes: {
        1: {
            points: [
                {latitude: '56.8354929', longitude: '60.590235199999995'},
                {latitude: '56.83', longitude: '60.5'},
                {latitude: '56.8', longitude: '60.9'}
            ]
        }
    }
}

var fakeFullData = {
    snakes: {
        '0': {
            points: [
                {latitude: '56.8354929', longitude: '60.590235199999995'},
                {latitude: '56.83', longitude: '60.5'},
                {latitude: '56.8', longitude: '60.9'}
            ]
        },
        '1': {
            points: [
                {latitude: '56.8354929', longitude: '62.590235199999995'},
                {latitude: '56.83', longitude: '62.5'},
                {latitude: '56.8', longitude: '62.9'}
            ]
        }
    },
    apples: [
        {latitude: '52.8354929', longitude: '60.590235199999995'},
        {latitude: '52.83', longitude: '60.5'},
        {latitude: '52.8', longitude: '60.9'}
    ]
}
