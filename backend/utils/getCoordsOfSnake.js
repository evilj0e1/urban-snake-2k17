import geoLib from 'geolib';

const PART_WIDTH = 10;
const DATA = {
    1: {
        points: [
            {latitude: '56.8354929', longitude: '60.590235199999995'},
            {latitude: '56.83', longitude: '60.5'},
            {latitude: '56.8', longitude: '60.9'}
        ]
    }
};

const getLastPoint = id => Object.assign({}, DATA[id].points[0]);
const getPoints = id => [...DATA[id].points];
const setPoints = (id, points = []) => {
    DATA[id].points = points;

    return DATA[id].points;
};

const getGeneratedPoints = (newPoint, previousPoint, countOfPartsToSplice, countOfPoints) => {
    const points = [];
    const delta = {
        latitude: (parseFloat(newPoint.latitude) - parseFloat(previousPoint.latitude)) / countOfPartsToSplice,
        longitude: (parseFloat(newPoint.longitude) - parseFloat(previousPoint.longitude)) / countOfPartsToSplice
    };

    for (var i = 0; i < countOfPoints; i++) {
        points.push({
            latitude: JSON.stringify(parseFloat(previousPoint.latitude) + parseFloat(i * delta.latitude)),
            longitude: JSON.stringify(parseFloat(previousPoint.longitude) + parseFloat(i * delta.longitude))
        });
    }

    return points;
};

const getCoordsOfSnake = ({id, point: newPoint}) => {
    const points = getPoints(id);
    const countOfPoints = points.length;

    const previousPoint = getLastPoint(id);
    const distance = geoLib.getDistance(previousPoint, newPoint);
    const countOfPartsToSplice = Math.floor(distance / PART_WIDTH);

    if (countOfPartsToSplice) {
        points.splice(
            (points.length - countOfPartsToSplice < 0) ?
                0 :
                points.length - countOfPartsToSplice, countOfPartsToSplice
        );

        const generatedPoints = getGeneratedPoints(
            newPoint,
            previousPoint,
            countOfPartsToSplice,
            countOfPoints
        );

        setPoints(id, generatedPoints);
    }

    return getPoints(id);
};

export {
    getCoordsOfSnake
};
